
CREATE TABLE zabbix.history ( day Date,  
                                itemid UInt64,  
                                clock DateTime,  
                                ns UInt32, 
                                value Int64,  
                                value_dbl Float64,  
                                value_str String 
                            ) ENGINE = MergeTree(day, (itemid, clock), 8192);

CREATE TABLE zabbix.history_buffer (day Date,  
                                itemid UInt64,  
                                clock DateTime,  
                                ns UInt32,  
                                value Int64,  
                                value_dbl Float64,  
                                value_str String ) ENGINE = Buffer(zabbix, history, 8, 30, 60, 9000, 60000, 256000, 256000000) ;


--in case of nanoseconds aren't used, then remove ns field
--add hostname and itemname fileds to table and indexing if you use them:

CREATE TABLE zabbix.history ( day Date,  
                                itemid UInt64,  
                                clock DateTime,  
                                ns UInt32, 
                                value Int64,  
                                value_dbl Float64,  
                                value_str String 
                                hostname String,
                                itemname String) ENGINE = MergeTree(day, (itemid, clock, hostname, itemname), 8192);
CREATE TABLE zabbix.history_buffer (day Date,  
                                itemid UInt64,  
                                clock DateTime,  
                                ns UInt32,  
                                value Int64,  
                                value_dbl Float64,  
                                value_str String,
                                hostname String,
                                itemname String ) ENGINE = Buffer(zabbix, history, 8, 30, 60, 9000, 60000, 256000, 256000000) ;

--if you wish to have trends:

CREATE MATERIALIZED VIEW zabbix.trends_uint 
    (`clock` DateTime('Europe/Moscow'), 
    `itemid` UInt64, 
    `num` AggregateFunction(count, Int64), 
    `value_min` AggregateFunction(min, Int64), 
    `value_avg` AggregateFunction(avg, Int64), 
    `value_max` AggregateFunction(max, Int64)) 
    ENGINE = AggregatingMergeTree() PARTITION BY toYYYYMM(clock) ORDER BY (clock, itemid) SETTINGS index_granularity = 8192 
    AS SELECT toStartOfHour(clock) AS clock, 
        itemid, 
        countState(value) AS num, 
        minState(value) AS value_min, 
        avgState(value) AS value_avg, 
        maxState(value) AS value_max 
        FROM zabbix.history GROUP BY clock, itemid
--select clock, itemid, countMerge(num)  AS num,  minMerge(value_min) as value_min, avgMerge(value_avg) AS value_avg, maxMerge(value_max) AS value_max FROM trends_uint GROUP BY clock, itemid

CREATE MATERIALIZED VIEW zabbix.trends 
    (`clock` DateTime('Europe/Moscow'), 
    `itemid` UInt64, 
    `num` AggregateFunction(count, Float64), 
    `value_min` AggregateFunction(min, Float64), 
    `value_avg` AggregateFunction(avg, Float64), 
    `value_max` AggregateFunction(max, Float64)) 
    ENGINE = AggregatingMergeTree() PARTITION BY toYYYYMM(clock) ORDER BY (clock, itemid) SETTINGS index_granularity = 8192 
    AS SELECT toStartOfHour(clock) AS clock, 
        itemid, countState(value_dbl) AS num, 
        minState(value_dbl) AS value_min, 
        avgState(value_dbl) AS value_avg, 
        maxState(value_dbl) AS value_max 
        FROM zabbix.history GROUP BY clock, itemid
--select clock, itemid, countMerge(num)  AS num,  minMerge(value_min) as value_min, avgMerge(value_avg) AS value_avg, maxMerge(value_max) AS value_max FROM trends GROUP BY clock, itemid
